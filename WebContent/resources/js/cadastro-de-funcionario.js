
var inicio = new Vue({
	el:"#cadastro",
    data: {
        funcionario:{
            nome: "",
            setor: { id: ""},
            salario: "",
            email: "",
            idade: ""
        },
        setores: []
    },
    created: function(){
        let id = new URL(location.href).searchParams.get('id');
        let vm =  this;
        if(id){
            vm.buscaFuncionario(id);
        }
        vm.buscaSetores();
    },
    methods:{
        criarFuncionario: function(){
			const vm = this;
            if (vm.funcionario.id){
                axios.put("/funcionarios/rs/funcionarios/"+vm.funcionario.id, vm.funcionario)
                .then(response => {window.location.href = '../index.html'})
            } else {
                axios.post("/funcionarios/rs/funcionarios", vm.funcionario)
                .then(response => {window.location.href = '../index.html'})
            }
			
		},
        buscaFuncionario: function(id){
			const vm = this;
			axios.get("/funcionarios/rs/funcionarios/"+id)
			.then(response => {vm.funcionario = response.data;
			}).catch(function (error) {
				vm.mostraAlertaErro("Erro interno", "Não foi possível listar funcionários.");
			}).finally(function() {
			});
		},
        buscaSetores: function(){
			const vm = this;
			axios.get("/funcionarios/rs/setores/")
			.then(response => {vm.setores = response.data;
			}).catch(function (error) {
				vm.mostraAlertaErro("Erro interno", "Não foi possível listar setores.");
			}).finally(function() {
			});
		},
    }
});

