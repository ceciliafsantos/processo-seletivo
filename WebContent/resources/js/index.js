var inicio = new Vue({
	el:"#inicio",
    data: {
        listaFuncionarios: [],
        listaFuncionariosHeader: [
			{sortable: false, key: "nome", label:"Nome"},
			{sortable: false, key: "setor.nome", label:"Setor"},
			{sortable: false, key: "salario", label:"Salario"},
			{sortable: false, key: "email", label:"Email"},
			{sortable: false, key: "idade", label:"Idade"}
		]
    },
    created: function(){
        let vm =  this;
        vm.buscaFuncionarios();
    },
    methods:{
        buscaFuncionarios: function(){
			const vm = this;
			axios.get("/funcionarios/rs/funcionarios")
			.then(response => {vm.listaFuncionarios = response.data;
			}).catch(function (error) {
				vm.mostraAlertaErro("Erro interno", "Não foi possível listar funcionários.");
			}).finally(function() {
			});
		},
		deletarFuncionario: function(id){
			const vm = this;
			axios.delete("/funcionarios/rs/funcionarios/"+id)
			.then(response => {
				vm.listaFuncionarios = vm.listaFuncionarios.filter(e => e.id != id)
			})
			.catch(function (error) {
				vm.mostraAlertaErro("Erro interno", "Não foi possível deletar funcionário.");
			}).finally(function() {
			});
		},
		editarFuncionario: function(id){
			window.location.href = 'pages/novo-funcionario.html?id=' +id
		}
    }
});
