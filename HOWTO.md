PASSO A PASSO

Criar banco de dados no MySQL Workbench;

Instalar e configurar Apache Tomcat e Maven;

No prompt de comando, entrar na pasta do projeto e executar "mvn package" para build;

Arquivo .war será criado dentro da pasta target (já criado);

Copiar esse arquivo para a pasta webapps, onde o Apache Tomcat foi salvo, no meu caso: C:\Program Files\Apache Software Foundation\Tomcat 9.0\webapps;

Uma pasta será criada com o mesmo nome desse arquivo;

Acessar o local em que o Apache está rodando, no meu caso http://127.0.0.1:8080/funcionarios/index.html;

Nessa página, o que está cadastrado no banco será listado. Botão de cadastrar novo abre o formulário (necessário setores no banco para que apareçam no select). Funções de inserir, editar e deletar criadas.